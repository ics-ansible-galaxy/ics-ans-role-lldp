import os
import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("all")


def test_lldpd_running_and_enabled(host):
    lldpd = host.service("lldpd")
    assert lldpd.is_running
    assert lldpd.is_enabled

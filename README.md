# ics-ans-role-lldp

Ansible role to install and enable lldp.

Note that to disable the internal lldp agent you can run:
Only on driver versions i40e 2.7.11+

ethtool --show-priv-flags <ifname> 
ethtool --set-priv-flags <ifname> disable-fw-lldp on 

Versions earlier than that can use this method:
echo lldp stop/start > /sys/kernel/debug/i40e/the pci adress/command

For example:
echo lldp stop > /sys/kernel/debug/i40e/0000\:19\:00.0/command

## Role Variables

```yaml
...
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-lldp
```

## License

BSD 2-clause

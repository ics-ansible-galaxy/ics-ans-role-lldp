#!/bin/bash
i40e_path=/sys/kernel/debug/i40e
for dev in $i40e_path/*; do
    [ -e "$dev" ] || break
    echo lldp stop > "${dev}/command"
done